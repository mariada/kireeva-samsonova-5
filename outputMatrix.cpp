#include <iostream>

using namespace std;

void outputMatrix(int** matrixA, short matrixSize)
{
    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            cout << matrixA[i][j] << "\t";
        }
        cout << endl;
    }
}